<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FlickrApiController extends Controller
{
    //
    public function index($page, $tags) {
        $url = "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=782f256cab3f792783b80e2b30e10569&tags=cats&format=json&nojsoncallback=1&per_page=20&page=".$page."&tags=".$tags;
        $data = json_decode(file_get_contents($url));
        $photos = $data->photos;

        return response()->json($photos, 200);
    }

    //
    public function search($tags) {
        $url = "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=782f256cab3f792783b80e2b30e10569&format=json&nojsoncallback=1&per_page=20&tags=".$tags;
        $data = json_decode(file_get_contents($url));
        $photos = $data->photos;

        return response()->json($photos, 200);
    }
}