<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index() {
        $url = "http://localhost/aiatest_flickr_api/public/api/test/1/cats";
        $data = json_decode(file_get_contents($url));

        return view('welcome', ['data' => $data]);
    }

    public function paging($page, $search) {
        $url = "http://localhost/aiatest_flickr_api/public/api/test/".$page."./".$search;
        $data = json_decode(file_get_contents($url));

        return view('welcome', ['data' => $data, 'search' => $search]);
    }

    public function search(Request $request) {
        $url = "http://localhost/aiatest_flickr_api/public/api/search/".$request->get('search');
        $data = json_decode(file_get_contents($url));

        return view('welcome', ['data' => $data, 'search' => $request->get('search')]);
    }
}